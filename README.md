# The Game of Railway Traffic Control :: Infrastructure

## About

The goal of simjop project is create open source game of railway traffic control.
If you want to be in touch with the community, please connect to [discord](https://discord.gg/drcmjBf).

## Run Server

### Prepare server
    # server configuration
    git clone https://gitlab.com/simjop/infra.git
    mkdir -p ~/.env/
    cp infra/template/production_server.env ~/.env/production_server.env
    cp infra/template/gitlab_runner.env ~/.env/gitlab_runner.env

    vim ~/.env/production_server.env
    vim ~/.env/gitlab_runner.env

### Operations

    # update docker images & restart docker-compose
    ./update_images.sh

    # server up
    source ~/.env/production_server.env
    sudo -E docker-compose --file ~/infra/deploy/docker-compose.yml up -d --build

    # server down
    source ~/.env/production_server.env
    sudo -E docker-compose --file ~/infra/deploy/docker-compose.yml down

    # remove all containers
    docker rmi -f $(docker images -a -q)
