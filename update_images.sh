#!/usr/bin/env bash

source ~/.env/production_server.env

#docker login -u $K8S_SECRET_REGISTRY_USER -p $K8S_SECRET_REGISTRY_TOKEN $CI_REGISTRY
echo $K8S_SECRET_REGISTRY_TOKEN | docker login $CI_REGISTRY --username $K8S_SECRET_REGISTRY_USER --password-stdin


docker pull registry.gitlab.com/simjop/web:${IMAGE_TAG_WEB}
docker pull registry.gitlab.com/simjop/editor

docker image ls

# server down
sudo -E docker-compose --file ~/infra/deploy/docker-compose.yml down

# server up
sudo -E docker-compose --file ~/infra/deploy/docker-compose.yml up -d --build

sleep 3
docker ps
